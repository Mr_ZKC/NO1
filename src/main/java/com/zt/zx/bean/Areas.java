package com.zt.zx.bean;

public class Areas {
    private Integer aId;

    private String area;

    private String cidSs;

    public Integer getaId() {
        return aId;
    }

    public void setaId(Integer aId) {
        this.aId = aId;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area == null ? null : area.trim();
    }

    public String getCidSs() {
        return cidSs;
    }

    public void setCidSs(String cidSs) {
        this.cidSs = cidSs == null ? null : cidSs.trim();
    }

    public Areas() {
    }

    public Areas(Integer aId, String area, String cidSs) {
        super();
        this.aId = aId;
        this.area = area;
        this.cidSs = cidSs;
    }
}
