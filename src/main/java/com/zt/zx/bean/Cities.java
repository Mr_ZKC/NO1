package com.zt.zx.bean;
import java.util.List;
public class Cities {
    private Integer cId;

    private String cidS;

    private String citys;

    private String pidSs;
    /*附加属性*/
    private List<Areas> areas;

    public List<Areas> getAreas() {
        return areas;
    }

    public void setAreas(List<Areas> areas) {
        this.areas = areas;
    }

    public Integer getcId() {
        return cId;
    }

    public void setcId(Integer cId) {
        this.cId = cId;
    }

    public String getCidS() {
        return cidS;
    }

    public void setCidS(String cidS) {
        this.cidS = cidS == null ? null : cidS.trim();
    }

    public String getcitys() {
        return citys;
    }

    public void setcitys(String citys) {
        this.citys = citys == null ? null : citys.trim();
    }

    public String getPidSs() {
        return pidSs;
    }

    public void setPidSs(String pidSs) {
        this.pidSs = pidSs == null ? null : pidSs.trim();
    }

    public Cities() {
    }

    public Cities(Integer cId, String cidS, String citys, String pidSs) {
        super();
        this.cId = cId;
        this.cidS = cidS;
        this.citys = citys;
        this.pidSs = pidSs;
    }

    public Cities(Integer cId, String cidS, String citys, String pidSs, List<Areas> areas) {
        this.cId = cId;
        this.cidS = cidS;
        this.citys = citys;
        this.pidSs = pidSs;
        this.areas = areas;
    }
}