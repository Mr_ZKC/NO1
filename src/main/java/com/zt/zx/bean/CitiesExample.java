package com.zt.zx.bean;

import java.util.ArrayList;
import java.util.List;

public class CitiesExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CitiesExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCIdIsNull() {
            addCriterion("c_id is null");
            return (Criteria) this;
        }

        public Criteria andCIdIsNotNull() {
            addCriterion("c_id is not null");
            return (Criteria) this;
        }

        public Criteria andCIdEqualTo(Integer value) {
            addCriterion("c_id =", value, "cId");
            return (Criteria) this;
        }

        public Criteria andCIdNotEqualTo(Integer value) {
            addCriterion("c_id <>", value, "cId");
            return (Criteria) this;
        }

        public Criteria andCIdGreaterThan(Integer value) {
            addCriterion("c_id >", value, "cId");
            return (Criteria) this;
        }

        public Criteria andCIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("c_id >=", value, "cId");
            return (Criteria) this;
        }

        public Criteria andCIdLessThan(Integer value) {
            addCriterion("c_id <", value, "cId");
            return (Criteria) this;
        }

        public Criteria andCIdLessThanOrEqualTo(Integer value) {
            addCriterion("c_id <=", value, "cId");
            return (Criteria) this;
        }

        public Criteria andCIdIn(List<Integer> values) {
            addCriterion("c_id in", values, "cId");
            return (Criteria) this;
        }

        public Criteria andCIdNotIn(List<Integer> values) {
            addCriterion("c_id not in", values, "cId");
            return (Criteria) this;
        }

        public Criteria andCIdBetween(Integer value1, Integer value2) {
            addCriterion("c_id between", value1, value2, "cId");
            return (Criteria) this;
        }

        public Criteria andCIdNotBetween(Integer value1, Integer value2) {
            addCriterion("c_id not between", value1, value2, "cId");
            return (Criteria) this;
        }

        public Criteria andCidSIsNull() {
            addCriterion("cid_s is null");
            return (Criteria) this;
        }

        public Criteria andCidSIsNotNull() {
            addCriterion("cid_s is not null");
            return (Criteria) this;
        }

        public Criteria andCidSEqualTo(String value) {
            addCriterion("cid_s =", value, "cidS");
            return (Criteria) this;
        }

        public Criteria andCidSNotEqualTo(String value) {
            addCriterion("cid_s <>", value, "cidS");
            return (Criteria) this;
        }

        public Criteria andCidSGreaterThan(String value) {
            addCriterion("cid_s >", value, "cidS");
            return (Criteria) this;
        }

        public Criteria andCidSGreaterThanOrEqualTo(String value) {
            addCriterion("cid_s >=", value, "cidS");
            return (Criteria) this;
        }

        public Criteria andCidSLessThan(String value) {
            addCriterion("cid_s <", value, "cidS");
            return (Criteria) this;
        }

        public Criteria andCidSLessThanOrEqualTo(String value) {
            addCriterion("cid_s <=", value, "cidS");
            return (Criteria) this;
        }

        public Criteria andCidSLike(String value) {
            addCriterion("cid_s like", value, "cidS");
            return (Criteria) this;
        }

        public Criteria andCidSNotLike(String value) {
            addCriterion("cid_s not like", value, "cidS");
            return (Criteria) this;
        }

        public Criteria andCidSIn(List<String> values) {
            addCriterion("cid_s in", values, "cidS");
            return (Criteria) this;
        }

        public Criteria andCidSNotIn(List<String> values) {
            addCriterion("cid_s not in", values, "cidS");
            return (Criteria) this;
        }

        public Criteria andCidSBetween(String value1, String value2) {
            addCriterion("cid_s between", value1, value2, "cidS");
            return (Criteria) this;
        }

        public Criteria andCidSNotBetween(String value1, String value2) {
            addCriterion("cid_s not between", value1, value2, "cidS");
            return (Criteria) this;
        }

        public Criteria andcitysIsNull() {
            addCriterion("citys is null");
            return (Criteria) this;
        }

        public Criteria andcitysIsNotNull() {
            addCriterion("citys is not null");
            return (Criteria) this;
        }

        public Criteria andcitysEqualTo(String value) {
            addCriterion("citys =", value, "citys");
            return (Criteria) this;
        }

        public Criteria andcitysNotEqualTo(String value) {
            addCriterion("citys <>", value, "citys");
            return (Criteria) this;
        }

        public Criteria andcitysGreaterThan(String value) {
            addCriterion("citys >", value, "citys");
            return (Criteria) this;
        }

        public Criteria andcitysGreaterThanOrEqualTo(String value) {
            addCriterion("citys >=", value, "citys");
            return (Criteria) this;
        }

        public Criteria andcitysLessThan(String value) {
            addCriterion("citys <", value, "citys");
            return (Criteria) this;
        }

        public Criteria andcitysLessThanOrEqualTo(String value) {
            addCriterion("citys <=", value, "citys");
            return (Criteria) this;
        }

        public Criteria andcitysLike(String value) {
            addCriterion("citys like", value, "citys");
            return (Criteria) this;
        }

        public Criteria andcitysNotLike(String value) {
            addCriterion("citys not like", value, "citys");
            return (Criteria) this;
        }

        public Criteria andcitysIn(List<String> values) {
            addCriterion("citys in", values, "citys");
            return (Criteria) this;
        }

        public Criteria andcitysNotIn(List<String> values) {
            addCriterion("citys not in", values, "citys");
            return (Criteria) this;
        }

        public Criteria andcitysBetween(String value1, String value2) {
            addCriterion("citys between", value1, value2, "citys");
            return (Criteria) this;
        }

        public Criteria andcitysNotBetween(String value1, String value2) {
            addCriterion("citys not between", value1, value2, "citys");
            return (Criteria) this;
        }

        public Criteria andPidSsIsNull() {
            addCriterion("pid_ss is null");
            return (Criteria) this;
        }

        public Criteria andPidSsIsNotNull() {
            addCriterion("pid_ss is not null");
            return (Criteria) this;
        }

        public Criteria andPidSsEqualTo(String value) {
            addCriterion("pid_ss =", value, "pidSs");
            return (Criteria) this;
        }

        public Criteria andPidSsNotEqualTo(String value) {
            addCriterion("pid_ss <>", value, "pidSs");
            return (Criteria) this;
        }

        public Criteria andPidSsGreaterThan(String value) {
            addCriterion("pid_ss >", value, "pidSs");
            return (Criteria) this;
        }

        public Criteria andPidSsGreaterThanOrEqualTo(String value) {
            addCriterion("pid_ss >=", value, "pidSs");
            return (Criteria) this;
        }

        public Criteria andPidSsLessThan(String value) {
            addCriterion("pid_ss <", value, "pidSs");
            return (Criteria) this;
        }

        public Criteria andPidSsLessThanOrEqualTo(String value) {
            addCriterion("pid_ss <=", value, "pidSs");
            return (Criteria) this;
        }

        public Criteria andPidSsLike(String value) {
            addCriterion("pid_ss like", value, "pidSs");
            return (Criteria) this;
        }

        public Criteria andPidSsNotLike(String value) {
            addCriterion("pid_ss not like", value, "pidSs");
            return (Criteria) this;
        }

        public Criteria andPidSsIn(List<String> values) {
            addCriterion("pid_ss in", values, "pidSs");
            return (Criteria) this;
        }

        public Criteria andPidSsNotIn(List<String> values) {
            addCriterion("pid_ss not in", values, "pidSs");
            return (Criteria) this;
        }

        public Criteria andPidSsBetween(String value1, String value2) {
            addCriterion("pid_ss between", value1, value2, "pidSs");
            return (Criteria) this;
        }

        public Criteria andPidSsNotBetween(String value1, String value2) {
            addCriterion("pid_ss not between", value1, value2, "pidSs");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}