package com.zt.zx.bean;

import java.util.List;

public class Provinces {
    private Integer pId;

    private Integer pidS;

    private String province;

    /*附加属性*/
    private List<Cities> cities;

    public List<Cities> getCities() {
        return cities;
    }

    public void setCities(List<Cities> cities) {
        this.cities = cities;
    }

    public Integer getpId() {
        return pId;
    }

    public void setpId(Integer pId) {
        this.pId = pId;
    }

    public Integer getPidS() {
        return pidS;
    }

    public void setPidS(Integer pidS) {
        this.pidS = pidS;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public Provinces() {
    }

    public Provinces(Integer pId, Integer pidS, String province, List<Cities> cities) {
        this.pId = pId;
        this.pidS = pidS;
        this.province = province;
        this.cities = cities;
    }


}