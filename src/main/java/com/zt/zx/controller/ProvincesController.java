package com.zt.zx.controller;

import com.zt.zx.bean.Msg;
import com.zt.zx.bean.Provinces;
import com.zt.zx.service.ProvincesService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class ProvincesController {



     @Autowired
     private ProvincesService provincesService;


     @RequestMapping("/getProvincesServiceJson")
     @ResponseBody
     public Msg getProvincesServiceJson(){
          List<Provinces> provinces=provincesService.getAll();


          return Msg.success().add("provinces",provinces);
     }





}
