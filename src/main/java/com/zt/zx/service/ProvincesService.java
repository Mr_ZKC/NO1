package com.zt.zx.service;


import com.zt.zx.bean.Provinces;
import com.zt.zx.dao.ProvincesMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProvincesService {



    @Autowired
    private ProvincesMapper provincesMapper;


    /*查询所有*/

    public List<Provinces> getAll(){
        return provincesMapper.selectByExamples(null);
    }



}
