<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    // 将path放到页作用域中，方便使用el表达式调用
    pageContext.setAttribute("app_path", path);
/* 如果 idea  pageContext.setAttribute("app_path", path); 爆红可以将一下代码放入pom.xml文件中   */
/*
    <dependency>
    <groupId>javax.servlet</groupId>
    <artifactId>javax.servlet-api</artifactId>
    <version>3.0.1</version>
    <scope>provided</scope>
    </dependency>
    <dependency>
    <groupId>javax.servlet.jsp</groupId>
    <artifactId>jsp-api</artifactId>
    <version>2.2</version>
    <scope>provided</scope>
    </dependency>
*/
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>1</title>
    <script type="text/javascript" src="https://cdn.staticfile.org/jquery/1.8.1/jquery.js"></script>
</head>
<body>
<div  style="width: 500px;height: 500px;margin: 0 auto;">
    <select id="province"><option id="yi" style="width: 20px;">---请选择---</option></select>
    <select id="citys"><option  style="width: 20px;">---请选择---</option></select>
    <select id="countys"><option  style="width: 20px;">---请选择---</option></select>
</div>
</body>
<script>
    $(function() {
        to_page();
    });
    var jsons;

    /*省*/
    function to_page() {
        $.ajax({
            url : "${app_path}/getProvincesServiceJson",
            dataType:"json",
            type : "get",
            success : function (result) {
                jsons=result;
                console.log(jsons);
                $.each(result.extend.provinces,function (i,item) {
                    $("<option></option>").append(item.province).attr("value",item.province+"-"+item.pidS).appendTo("#province");
                })
            }
        });
    }
    /*市*/
    /*   获取点击下拉框后的值  用 change  */
    $("#province").change(function () {
        $("#citys").empty();
        $("#countys").empty();
        $("#yi").remove();
        var i=$("#province").val();
        var site = i.lastIndexOf("-");
        var pidS=i.substring(site+1,i.length);
        $("#citys").append($("<option id='er'></option>").append("---请选择---"));
        $("#countys").append($("<option></option>").append("---请选择---"));
        $.each(jsons.extend.provinces,function (i,item) {
            $.each(item.cities,function (k,items) {

                if(items.pidSs == pidS){

                    $("<option></option>").append(items.citys).attr("value",items.pidSs+"-"+items.citys+"-"+items.cidS).appendTo("#citys");
                }
            })
        })
    })
    /*区*/

    /*   获取点击下拉框后的值  用 change  */
    $("#citys").change(function () {
        $("#countys").empty();
        $("#er").remove();
        var i=$("#citys").val();
        var site = i.lastIndexOf("-");
        var cidS=i.substring(site+1,i.length);
        $("#countys").append($("<option id='san'></option>").append("---请选择---"));
        $.each(jsons.extend.provinces,function (i,item) {
            $.each(item.cities,function (k,items) {
                $.each(items.areas,function (k,itemss) {
                    if(itemss.cidSs == cidS){
                        $("<option></option>").append(itemss.area).appendTo("#countys");
                    }
                })
            })
        })
    })

    $("#countys").change(function () {
        $("#san").remove();
    })







    /*  用户获取跟目录 */
    function getRootPath(){
        var curPageUrl = window.document.location.href;
        var rootPath = curPageUrl.split("//")[0] + curPageUrl.split("//")[1].split("/")[0]
            + "/" + curPageUrl.split("//")[1].split("/")[1];
        return rootPath;
    }
    console.log(111111111111+getRootPath());
</script>

</html>

























